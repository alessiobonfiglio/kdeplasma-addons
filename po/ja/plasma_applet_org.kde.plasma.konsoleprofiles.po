msgid ""
msgstr ""
"Project-Id-Version: konsoleprofiles\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-06-16 00:49+0000\n"
"PO-Revision-Date: 2021-10-16 18:27+0900\n"
"Last-Translator: R.Suga <21r.suga@gmail.com>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Generator: Poedit 2.4.2\n"

#: package/contents/ui/konsoleprofiles.qml:61
#, kde-format
msgctxt "@title"
msgid "Konsole Profiles"
msgstr "Konsole プロファイル"

#: package/contents/ui/konsoleprofiles.qml:81
#, kde-format
msgid "Arbitrary String Which Says Something"
msgstr "何かを言う任意の文字列"
