# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kdeplasma-addons package.
# Wantoyo <wantoyek@gmail.com>, 2018, 2019, 2022.
# Aziz Adam Adrian <4.adam.adrian@gmail.com>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: kdeplasma-addons\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-23 00:48+0000\n"
"PO-Revision-Date: 2022-06-07 14:40+0700\n"
"Last-Translator: Aziz Adam Adrian <4.adam.adrian@gmail.com>\n"
"Language-Team: Indonesian <kde-i18n-doc@kde.org>\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.04.1\n"

#: package/contents/config/config.qml:13
#, kde-format
msgctxt "@title"
msgid "Appearance"
msgstr "Penampilan"

#: package/contents/ui/configAppearance.qml:30
#, kde-format
msgid "%1pt"
msgstr "%1pt"

#: package/contents/ui/configAppearance.qml:36
#, kde-format
msgid "Text font size:"
msgstr "Ukuran teks font:"

#: package/contents/ui/configAppearance.qml:41
#, kde-format
msgid "Background color"
msgstr "Warna latarbelakang"

#: package/contents/ui/configAppearance.qml:77
#, kde-format
msgid "A white sticky note"
msgstr "Sebuah catatan lekat putih"

#: package/contents/ui/configAppearance.qml:78
#, kde-format
msgid "A black sticky note"
msgstr "Sebuah catatan lekat hitam"

#: package/contents/ui/configAppearance.qml:79
#, kde-format
msgid "A red sticky note"
msgstr "Sebuah catatan lekat merah"

#: package/contents/ui/configAppearance.qml:80
#, kde-format
msgid "An orange sticky note"
msgstr "Sebuah catatan lekat jingga"

#: package/contents/ui/configAppearance.qml:81
#, kde-format
msgid "A yellow sticky note"
msgstr "Sebuah catatan lekat kuning"

#: package/contents/ui/configAppearance.qml:82
#, kde-format
msgid "A green sticky note"
msgstr "Sebuah catatan lekat hijau"

#: package/contents/ui/configAppearance.qml:83
#, kde-format
msgid "A blue sticky note"
msgstr "Sebuah catatan lekat biru"

#: package/contents/ui/configAppearance.qml:84
#, kde-format
msgid "A pink sticky note"
msgstr "Sebuah catatan lekat jambon"

#: package/contents/ui/configAppearance.qml:85
#, kde-format
msgid "A translucent sticky note"
msgstr "Sebuah catatan lekat translusen"

#: package/contents/ui/configAppearance.qml:86
#, kde-format
msgid "A translucent sticky note with light text"
msgstr "Sebuah catatan lekat translusen dengan teks terang"

#: package/contents/ui/main.qml:250
#, kde-format
msgid "Undo"
msgstr "Urung"

#: package/contents/ui/main.qml:258
#, kde-format
msgid "Redo"
msgstr "Ulang"

#: package/contents/ui/main.qml:268
#, kde-format
msgid "Cut"
msgstr "Potong"

#: package/contents/ui/main.qml:276
#, kde-format
msgid "Copy"
msgstr "Salin"

#: package/contents/ui/main.qml:284
#, kde-format
msgid "Paste Without Formatting"
msgstr "Tempel Tanpa Memformat"

#: package/contents/ui/main.qml:290
#, kde-format
msgid "Paste"
msgstr "Tempel"

#: package/contents/ui/main.qml:299
#, kde-format
msgid "Delete"
msgstr "Hapus"

#: package/contents/ui/main.qml:306
#, kde-format
msgid "Clear"
msgstr "Bersihkan"

#: package/contents/ui/main.qml:316
#, kde-format
msgid "Select All"
msgstr "Pilih Semua"

#: package/contents/ui/main.qml:444
#, kde-format
msgctxt "@info:tooltip"
msgid "Bold"
msgstr "Tebal"

#: package/contents/ui/main.qml:456
#, kde-format
msgctxt "@info:tooltip"
msgid "Italic"
msgstr "Miring"

#: package/contents/ui/main.qml:468
#, kde-format
msgctxt "@info:tooltip"
msgid "Underline"
msgstr "Garisbawahi"

#: package/contents/ui/main.qml:480
#, kde-format
msgctxt "@info:tooltip"
msgid "Strikethrough"
msgstr "Coret"

#: package/contents/ui/main.qml:554
#, kde-format
msgid "Discard this note?"
msgstr "Buang catatan ini?"

#: package/contents/ui/main.qml:555
#, kde-format
msgid "Are you sure you want to discard this note?"
msgstr "Apakah kamu yakin ingin membuang catatan ini?"

#: package/contents/ui/main.qml:568
#, kde-format
msgctxt "@item:inmenu"
msgid "White"
msgstr "Putih"

#: package/contents/ui/main.qml:569
#, kde-format
msgctxt "@item:inmenu"
msgid "Black"
msgstr "Hitam"

#: package/contents/ui/main.qml:570
#, kde-format
msgctxt "@item:inmenu"
msgid "Red"
msgstr "Merah"

#: package/contents/ui/main.qml:571
#, kde-format
msgctxt "@item:inmenu"
msgid "Orange"
msgstr "Jingga"

#: package/contents/ui/main.qml:572
#, kde-format
msgctxt "@item:inmenu"
msgid "Yellow"
msgstr "Kuning"

#: package/contents/ui/main.qml:573
#, kde-format
msgctxt "@item:inmenu"
msgid "Green"
msgstr "Hijau"

#: package/contents/ui/main.qml:574
#, kde-format
msgctxt "@item:inmenu"
msgid "Blue"
msgstr "Biru"

#: package/contents/ui/main.qml:575
#, kde-format
msgctxt "@item:inmenu"
msgid "Pink"
msgstr "Jambon"

#: package/contents/ui/main.qml:576
#, kde-format
msgctxt "@item:inmenu"
msgid "Translucent"
msgstr "Bening"

#: package/contents/ui/main.qml:577
#, kde-format
msgctxt "@item:inmenu"
msgid "Translucent Light"
msgstr "Bening Terang"
